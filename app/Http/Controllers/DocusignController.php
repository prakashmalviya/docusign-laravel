<?php

namespace App\Http\Controllers;

use App\Http\Requests\DocumentVerification;
use Illuminate\Http\Request;
use DocuSign\eSign\Api\EnvelopesApi;
use DocuSign\eSign\Client\ApiClient;
use DocuSign\eSign\Client\ApiException;
use DocuSign\eSign\Model\Document;
use DocuSign\eSign\Model\SignHere;
use DocuSign\eSign\Model\DateSigned;
use DocuSign\eSign\Model\FullName;
use DocuSign\eSign\Model\Text;
use DocuSign\eSign\Model\Email;
use DocuSign\eSign\Model\Tabs;
use DocuSign\eSign\Model\Signer;
use DocuSign\eSign\Model\Recipients;
use DocuSign\eSign\Model\InlineTemplate;
use DocuSign\eSign\Model\CompositeTemplate;
use DocuSign\eSign\Model\EnvelopeDefinition;
use Session;

class DocusignController extends Controller
{

    private $config, $args, $signer_client_id = 1000;

    /**
     * Show the html page
     *
     * @param Request $request
     *
     * @return render
     */
    public function index(DocumentVerification $request)
    {

        try
        {
            $fileContent = $request->file('formFile')->get();
            $file        = $request->file('formFile');
            $fileName    = $file->getClientOriginalName();
            $extension   = pathinfo($fileName, PATHINFO_EXTENSION);
            $nameOfFile  = pathinfo($fileName, PATHINFO_FILENAME);
            $data        = [
                'name'         => $request->name,
                'email'        => $request->email,
                'extension'    => $extension,
                'custom'       => $request->custom,
                'file_name'    => $nameOfFile,
                'file'         => $fileName,
                'file_content' => $fileContent,
            ];
            $request->session()->put('data', $data);
            return view('connect');
        }
        catch (\Exception $e)
        {
            return back()->withError($e->getMessage());
        }

    }

    /**
     * Connect your application to docusign
     *
     * @param Request $request
     *
     * @return url
     * @throws ApiException
     */
    public function connectDocusign(Request $request)
    {
        /**
         *
         * Step 2
         * Instantiate the eSign API client and set the OAuth path used by the JWT request
         *
         * Generate a new JWT access token
         *
         */
        $apiClient = new ApiClient();
        $apiClient->getOAuth()->setOAuthBasePath(env('DS_AUTH_SERVER'));
        try
        {
            $accessToken = $this->getToken($apiClient);

        }
        catch (\Throwable $th)
        {
            $request->session()->flash('danger', $th->getMessage());
            return back()->withInput();
        }
        /**
         *
         * Step 3
         * Get user's info i.e. accounts array and base path
         *
         * Update the base path. The result in demo will be https://demo.docusign.net/restapi
         * User default account is always first in the array
         *
         */
        $userInfo    = $apiClient->getUserInfo($accessToken);
        $accountInfo = $userInfo[0]->getAccounts();
        $apiClient->getConfig()->setHost($accountInfo[0]->getBaseUri() . env('DS_ESIGN_URI_SUFFIX'));
        /**
         *
         * Step 4
         * Build the envelope object
         *
         * Make an API call to create the envelope and display the response in the view
         *
         */
        $envelopeDefenition = $this->buildEnvelope($request);

        try
        {
            $envelopeApi = new EnvelopesApi($apiClient);
            $result      = $envelopeApi->createEnvelope($accountInfo[0]->getAccountId(), $envelopeDefenition);
            $envelopeId  = $result->getEnvelopeId();
            $request->session()->put('envelope_id', $envelopeId);
        }
        catch (\Throwable $th)
        {
            $request->session()->flash('danger', $th->getMessage());
            return back()->withError($th->getMessage())->withInput();
        }

        return redirect()->route('connect.docusign')->with('result', $result);


    }

    /**
     * @param Request $request
     *
     * @return EnvelopeDefinition
     */
    private function buildEnvelope(Request $request): EnvelopeDefinition
    {
        $data           = session()->get('data');
        $fileContent    = $data['file_content'];
        $fileName       = $data['file_name'];
        $fileExtension  = $data['extension'];
        $recipientEmail = $data['email'] ?? '';
        $address        = $data['custom'] ?? '';
        $recipientName  = $data['name'] ?? '';

        $document = new Document([
            'document_id'     => "1",
            'document_base64' => base64_encode($fileContent),
            'file_extension'  => $fileExtension,
            'name'            => $fileName
        ]);

        $sign_here_tab = new SignHere([
            'anchor_string'   => "Signature of Owner or Authorized Agent of Owner", //Must be match this text somewhere in your document OR you may replace it according your requirement
            'anchor_units'    => "pixels",
            'anchor_x_offset' => "25",
            'anchor_y_offset' => "-20",
            'font_size'       => "Size11",
            'font'            => "Arial",
            'font_color'      => 'Black'
        ]);

        $full_name = new FullName([
            'anchor_string'   => "Printed Name of Owner or Authorized Agent of Owner",  //Must be match this text somewhere in your document OR you may replace it according your requirement
            'anchor_units'    => "pixels",
            'anchor_x_offset' => "0",
            'anchor_y_offset' => "-20",
            'value'           => $recipientName,
            'font_size'       => "Size11",
            'font'            => "Arial",
            'font_color'      => 'Black'
        ]);

        $date = new DateSigned([
            'anchor_string'   => "Date of Sign",  //Must be match this text somewhere in your document OR you may replace it according your requirement
            'anchor_units'    => "pixels",
            'anchor_x_offset' => "0",
            'anchor_y_offset' => "-20",
            'value'           => date('m/d/Y'),
            'font_size'       => "Size11",
            'font'            => "Arial",
            'font_color'      => 'Black'
        ]);

        $text_tabs = new Text([
            'anchor_string'   => "Custom Text", //Must be match this text somewhere in your document OR you may replace it according your requirement
            'anchor_units'    => "pixels",
            'anchor_x_offset' => "85",
            'anchor_y_offset' => "-5",
            'value'           => $address,
            'font_size'       => "Size11",
            'font'            => "Arial",
            'font_color'      => 'Black'
        ]);

        $email_tabs = new Email([
            'anchor_string'   => "Email", //Must be match this text somewhere in your document OR you may replace it according your requirement
            'anchor_units'    => "pixels",
            'anchor_x_offset' => "25",
            'anchor_y_offset' => "-5",
            'value'           => $recipientEmail,
            'font_size'       => "Size11",
            'font'            => "Arial",
            'font_color'      => 'Black'

        ]);

        $sign_here_tabs = [$sign_here_tab];
        $tabs1          = new Tabs([
            'sign_here_tabs' => $sign_here_tabs,
            'text_tabs'      => [$text_tabs],
            'date_tabs'      => [$date],
            'full_name_tabs' => [$full_name],
            'email_tabs'     => [$email_tabs],

        ]);
        $signer         = new Signer([
            'email'        => $recipientEmail,
            'name'         => $recipientName,
            'recipient_id' => "1",
            'tabs'         => $tabs1
        ]);
        $signers        = [$signer];

        $recipients = new Recipients([
            'signers' => $signers
        ]);

        $inline_template = new InlineTemplate([
            'recipients' => $recipients,
            'sequence'   => "1"
        ]);

        $inline_templates    = [$inline_template];
        $composite_template  = new CompositeTemplate([
            'composite_template_id' => "1",
            'document'              => $document,
            'inline_templates'      => $inline_templates
        ]);
        $composite_templates = [$composite_template];

        $envelope_definition = new EnvelopeDefinition([
            'composite_templates' => $composite_templates,
            'email_subject'       => "Please sign",
            'status'              => "sent"
        ]);

        return $envelope_definition;

    }


    /**
     * @param ApiClient $apiClient
     *
     * @return string
     */
    private function getToken(ApiClient $apiClient): string
    {
        try
        {
            $privateKey  = file_get_contents(storage_path(env('DS_KEY_PATH')), true);
            $response    = $apiClient->requestJWTUserToken(
                $ikey = env('DS_CLIENT_ID'),
                $userId = env('DS_IMPERSONATED_USER_ID'),
                $key = $privateKey,
                $scope = env('DS_JWT_SCOPE')
            );
            $token       = $response[0];
            $accessToken = $token->getAccessToken();
        }
        catch (\Throwable $th)
        {
            throw $th;
        }
        return $accessToken;
    }

}
