<?php
return [
    'api_token'  => env('DRIP_API_TOKEN', ''),
    'account_id' => env('DRIP_ACCOUNT_ID', ''),
];
