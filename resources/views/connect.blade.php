<!DOCTYPE html>
<html lang="en">
<head>
    <title>Docusign Integration Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif
        @foreach (['danger', 'success'] as $status)
            @if (Session::has($status))
                <div class="w-100 text-center">
                    <p class="alert alert-{{ $status }}">{{ Session::get($status) }}</p>
                </div>
            @endif
        @endforeach
    <div class="card  mt-5">
        <div class="card-header">
            Docusign Demo
        </div>
        <div class="card-body">
            <h5 class="card-title">Docusign Tutorial</h5>
            <p class="card-text">Click the button below to connect your application with docusign</p>
                <a href="{{ route('connect.docusign') }}" class=" btn btn-primary">Connect Docusign</a>
        </div>
    </div>
</div>
</body>
